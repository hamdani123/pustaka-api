package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.GET("/", rootHandler)

	router.GET("/hello", helloHandler)

	router.Run(":8080")
}
func rootHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"name": "Hamdani",
		"bio":  "belajar golang web-api",
	})
}
func helloHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"title":    "hello . . . . hamdani",
		"subtitle": "bismillah",
	})
}
